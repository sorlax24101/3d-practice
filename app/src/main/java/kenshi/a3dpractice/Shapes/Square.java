package kenshi.a3dpractice.Shapes;

import android.opengl.GLES30;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

public class Square extends Shape {


    private final static float coordinates[] = {
            -0.5f,  0.5f, 0.0f,   // top left
            -0.5f, -0.5f, 0.0f,   // bottom left
            0.5f, -0.5f, 0.0f,   // bottom right
            0.5f,  0.5f, 0.0f   // top right
    };

    private final static int coordinatesPerVertex = 3;

    private final short drawOrder[] = { 0, 1, 2, 0, 2, 3 }; // order to draw vertices

    private final ShortBuffer drawListBuffer;

    private float color[] = { 0.63671875f, 0.76953125f, 0.22265625f, 0.0f };
    private final int vertexStride = coordinatesPerVertex * 4; // 4 bytes per vertex

    public Square() {
        //todo: initialize square
        super(coordinates, coordinatesPerVertex);
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(drawOrder.length*2);
        byteBuffer.order(ByteOrder.nativeOrder());
        drawListBuffer = byteBuffer.asShortBuffer();
        drawListBuffer.put(drawOrder);
        drawListBuffer.position(0);
    }

    public void draw(float[]mvpMatrix) {
        coordinatesPreparation(coordinatesPerVertex, mvpMatrix, color);

        GLES30.glDrawElements(
                GLES30.GL_TRIANGLES,
                drawOrder.length,
                GLES30.GL_UNSIGNED_SHORT,
                drawListBuffer
        );

        GLES30.glDisableVertexAttribArray(positionHandler);
    }

    public void setColor(float color[]) { this.color = color; }
}
