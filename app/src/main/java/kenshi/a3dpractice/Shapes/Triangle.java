package kenshi.a3dpractice.Shapes;

import android.opengl.GLES30;

public class Triangle extends Shape {

    private final static float coordinates[] = {
            0.0f,  0.622008459f, 0.0f, // top
            -0.5f, -0.311004243f, 0.0f, // bottom left
            0.5f, -0.311004243f, 0.0f  // bottom right
    };
    private float color[] = { 0.63671875f, 0.76953125f, 0.22265625f, 0.0f };

    private final static int coordinatePerVertex = 3;
    private final int vertexCount = coordinates.length / coordinatePerVertex;

    public Triangle() {
        super(coordinates, coordinatePerVertex);
    }

    public void draw(float[]mvpMatrix) {
        coordinatesPreparation(coordinatePerVertex, mvpMatrix, color);
        GLES30.glDrawArrays(GLES30.GL_TRIANGLES, 0, vertexCount);
        GLES30.glDisableVertexAttribArray(program);
    }

    public void setColor(float color[]) { this.color = color; }
}
