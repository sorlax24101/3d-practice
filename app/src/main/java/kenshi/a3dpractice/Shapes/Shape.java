package kenshi.a3dpractice.Shapes;

import android.opengl.GLES30;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import kenshi.a3dpractice.Renderer;

abstract class Shape {
    private final String vertexShaderCode =
            // This matrix member variable provides a hook to manipulate
            // the coordinates of objects that use this vertex shader.
            "uniform mat4 uMVPMatrix;" +
                    "attribute vec4 vPosition;" +
                    "void main() {" +

                    // The matrix must be included as part of gl_Position
                    // Note that the uMVPMatrix factor *must be first* in order
                    // for the matrix multiplication product to be correct.
                    "gl_Position = uMVPMatrix * vPosition;" +
                    "}",

            fragmentShaderCode =
                    "precision mediump float;" +
                            "uniform vec4 vColor;" +
                            "void main() {" +
                            "gl_FragColor = vColor;" +
                            "}";

    private final String vectorPosition = "vPosition",
            color = "vColor";

    private final int vertexStride;
    final int program;
    private final FloatBuffer vertexBuffer;

    int positionHandler, colorHandler, mvpMatrixHandler;

    /**
     * @Description: initialize the OpenGL programs and create OpenGL executables
     * @param coordinates the coordinates of the shape
     * @param coordinatePerVertex
     */
    Shape(float[] coordinates, int coordinatePerVertex) {
        // 4 bytes per vertex
        vertexStride = coordinatePerVertex * 4;

        //initialize vertex byte buffer for shape coordinates
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(coordinates.length * 4);

        //use the device hardware byte order
        byteBuffer.order(ByteOrder.nativeOrder());

        //create a floating point buffer from byte buffer
        vertexBuffer = byteBuffer.asFloatBuffer();

        //add the coordinates to the float buffer
        vertexBuffer.put(coordinates);

        //set the buffer to read first coordinates
        vertexBuffer.position(0);

        //prepare shaders and OpenGL program
        int vertexShader = Renderer.loadShader(GLES30.GL_VERTEX_SHADER, vertexShaderCode);
        int fragmentShader = Renderer.loadShader(GLES30.GL_FRAGMENT_SHADER, fragmentShaderCode);

        program = GLES30.glCreateProgram(); // create empty opengl program
        GLES30.glAttachShader(program, vertexShader); // add vertex shader to the program
        GLES30.glAttachShader(program, fragmentShader); // add fragment shader to the program
        GLES30.glLinkProgram(program); // create OpenGL program executables
    }

    void coordinatesPreparation(int coordinatePerVertex, float[]mvpMatrix, float color[]) {
        // add program to OpenGL environment
        GLES30.glUseProgram(program);

        //get handler to vertex shader position member in shader code
        positionHandler = GLES30.glGetAttribLocation(program, vectorPosition);

        //enable a handle to the shape vertices
        GLES30.glEnableVertexAttribArray(positionHandler);

        // prepare shape coordinates data
        GLES30.glVertexAttribPointer (
                positionHandler,
                coordinatePerVertex,
                GLES30.GL_FLOAT,
                false,
                vertexStride,
                vertexBuffer
        );

        // get handle to fragment shader's vColor member
        colorHandler = GLES30.glGetUniformLocation(program, this.color);

        // Set color for drawing the triangle
        GLES30.glUniform4fv(colorHandler, 1, color, 0);

        //get handle to fragment shader's color member in shader code
        colorHandler = GLES30.glGetUniformLocation(program, "uMVPMatrix");
        Renderer.checkGLError("glGetUniformLocation");

        //apply projection and view transformation
        GLES30.glUniformMatrix4fv(mvpMatrixHandler, 1, false, mvpMatrix, 0);
    }
}
